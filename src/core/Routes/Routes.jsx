import { Route, Switch } from 'react-router-dom';


import { LoginPage } from '../../Pages/LoginPage/LoginPage'
import { EmployeePage } from '../../Pages/EmployeePage/EmployeePage';



export function Routes() {

return(
    <Switch> 
        <Route path="/employee">
            <EmployeePage/>
        </Route>

        <Route path="/">
            <LoginPage/>
        </Route>   
    </Switch>
);

} 