import { useState } from 'react'
import './AccordionUser.scss'
import { ChartBar } from '../ChartBar/ChartBar';
export function AccordionUser() {
    const [open, setOpen] = useState(false)
    const [accordion, setAccordion] = useState(true)
    const openAccordionUser = () => {
        setOpen(true)
        setAccordion(false)
    }
    const closeAccordionUser = () => {
        setOpen(false)
        setAccordion(true)
    }


    return (
        <>
            <div className={accordion ? 'c-accordionUser-closed' : 'c-accordionUser'}>
                {accordion &&
                    <div className="b-btn b-btn--small " onClick={openAccordionUser}></div>
                }
                {open &&
                    <div>
                        {/* // Contributions */}
                        <div className="c-accordionUser__contributions">
                            <h4 className="c-accordionUser__title">Tus aportaciones</h4>
                            {/* Slides / grafico mensual / descripción / clasificación de usuario*/}
                            <div className="c-accordionUser__graphic">
                                <ChartBar />
                            </div>
                            <div className="c-accordionUser__lastPickUp">
                                <p className="c-accordionUser__lastPickUpTitle">Ultima recogida:</p>
                                <div className="c-accordionUser__lastPickUpInfo">
                                    <p>10kg de cartón</p>
                                    <p>5kg de vidrio</p>
                                </div>
                            </div>
                        </div>

                        {/* // List */}
                        <ul className="c-accordionUser__uList mt-3">
                            <li className="c-accordionUser__list">
                                <span className="c-accordionUser__listPlain">
                                    Has reciclado suficiente cartón como para
                                        <span className="c-accordionUser__listEmphasis">
                                        imprimir un libro de 200 páginas
                                        </span>
                                </span>
                            </li>
                        </ul>

                        {/* // Images */}
                        <div className="c-accordionUser__images">
                            <div className="c-accordionUser__image">

                            </div>
                            <div className="c-accordionUser__image">

                            </div>
                        </div>

                        {/* // Close BTN */}
                        <div className="b-btn b-btn--dark b-btn--small mt-3" onClick={closeAccordionUser}>
                        </div>
                    </div>
                }
            </div>
        </>
    )
}