import axios from "axios";
import moment from "moment";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Rciclo from "../../assets/imgs/Logo_R-CICLO.png"

import './EmployeePage.scss';


export function EmployeePage() {
    const { register, handleSubmit, watch, reset, errors } = useForm();
    const [employee, setEmploye] = useState([])
    const [pickUps, setPickUps] = useState()
    const [counter, setCounter] = useState(-1)
    const [showAll, setShowAll] = useState(false)

    const getEmploye = () => {
        axios('https://r-ciclo.herokuapp.com/employee/' + document.cookie)
            .then(res => {
                setEmploye(res.data.results);
                if (employee.email != undefined) {
                    axios('https://r-ciclo.herokuapp.com/employee/pickUps/' + employee.email)
                        .then(res => {
                            setPickUps(res.data.results);
                        })
                        .catch((err) => { console.log(err); })
                }
            })
            .catch((err) => { console.log(err); })
    }

    //! Función para modificar el estado del pedido
    const modifyPickUpStatus = (i) => {

        // console.log(pickUps[i]._id);
        if (pickUps !== undefined && typeof i === 'number') {
            axios.patch('https://r-ciclo.herokuapp.com/employee/pickUps/' + pickUps[i]._id, { status: !pickUps[i].status })
                .then((res) => {
                    alert('La recogida programada por: ' + res.data.results.petitionMadeBy + ', se ha modificado a: Estado: (' + !res.data.results.status + ')')
                })
                .catch((err) => { console.log(err); })
        }


    }
    const modifyPickUpIsComming = (i) => {
        // console.log(pickUps[i]._id);
        if (pickUps !== undefined && typeof i === 'number') {
            axios.patch('https://r-ciclo.herokuapp.com/employee/pickUps/' + pickUps[i]._id, { isComming: !pickUps[i].isComming })
                .then((res) => {
                    alert('La recogida programada por: ' + res.data.results.petitionMadeBy + ', se ha modificado a: En camino (' + !res.data.results.isComming + ')')
                })
                .catch((err) => { console.log(err); })
        }
    }



    useEffect(getEmploye, [employee])

    return (
        <div className="c-employee">

            <img className="c-employee__logo" src={Rciclo}/>

            <div>
                <p className="">Bienvenido {employee.name} !</p>
                <p>Aqui podrás ver las recogidas</p><p>programadas para ti:</p>
                <div className="c-employee__truck">
                    <span className="i-truck i-truck--light"></span>
                </div>
                
                <div className="mt-4">
                    {pickUps !== undefined && <h2>Recogidas pendientes:</h2>}
                    {showAll && <button className="b-btn" onClick={() => { setShowAll(false) }}>Muestra completas</button>}
                    {!showAll && <button className="b-btn" onClick={() => { setShowAll(true) }}>Muestra todas</button>}
                    {pickUps != undefined && pickUps.map((pickUp, i) => {
                        return (
                            <div key={i}>
                                {(pickUp.status || showAll) &&
                                    <div className={pickUp.status ? "c-accordion" : "c-accordion c-accordion--done"}>

                                        <div className="c-accordion__title" onClick={() => { setCounter(i) }}>{pickUp.petitionMadeBy}</div>
                                        {counter === i &&
                                            <div>

                                                <div className="c-accordion__text">
                                                    <p><span>Ciudad:</span>  {pickUp.whereGoToCollect.city} {pickUp.whereGoToCollect.postalcode}</p>
                                                    <p><span>Calle:</span>  {pickUp.whereGoToCollect.street} {pickUp.whereGoToCollect.number}, {pickUp.whereGoToCollect.floor}</p>
                                                    <p><span>Día y hora de la recogida:</span> {moment(pickUp.date).format('MM/DD/YYYY hh:mm')}</p>
                                                    <p><span>Número de tléfono:</span> {pickUp.whereGoToCollect.phone}</p>
                                                </div>
                                                <div className="c-accordion__text">
                                                    <p><span>Comentario:</span>  {pickUp.coment}</p>
                                                </div>

                                                <div className="c-accordion__text c-accordion__text--bigger">Hay que recoger los siguientes productos: </div>
                                                <div>
                                                    {pickUp.objectToCollect.paper && <p className="c-accordion__text">- Cartón y papel</p>}
                                                    {pickUp.objectToCollect.glass && <p className="c-accordion__text">- Vidrio</p>}
                                                    {pickUp.objectToCollect.battery && <p className="c-accordion__text">- Baterías</p>}
                                                    {pickUp.objectToCollect.clothe && <p className="c-accordion__text">- Ropa</p>}
                                                    {pickUp.objectToCollect.oil && <p className="c-accordion__text">- Aceite</p>}
                                                </div>
                                                <div className="c-accordion__btn">
                                                    {!pickUp.isComming && <button onClick={() => { modifyPickUpIsComming(i) }} className="b-btn b-btn--dark mb-3">¡Voy de camino!</button>}
                                                    {pickUp.isComming && <button onClick={() => { modifyPickUpIsComming(i) }} className="b-btn b-btn--dark mb-3">No estoy de camino</button>}
                                                    {pickUp.status && <button onClick={() => { modifyPickUpStatus(i) }} className="b-btn b-btn--dark mb-1">¡Ya lo he recogido!</button>}
                                                    {!pickUp.status && <button onClick={() => { modifyPickUpStatus(i) }} className="b-btn b-btn--dark mb-1">No lo he recogido</button>}
                                                </div>
                                                <div className="c-accordion__btnClose b-btn b-btn--dark b-btn--small my-3" onClick={() => { setCounter(-1) }}>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                }
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}