import {useForm} from "react-hook-form";
import {NavLink, useHistory} from "react-router-dom";
import { PageContext } from "../../shared/context/PageContext";

import Rciclo from '../../assets/imgs/Logo_R-CICLO.png'

import './LoginPage.scss';
import { useContext } from "react";
import axios from "axios";

export function LoginPage() {
    const {register, handleSubmit, watch, reset, errors} = useForm();
    const {page, setPage} = useContext(PageContext);
    const history = useHistory();

    const onSubmit = (formData) => {
        console.log(formData);
        axios.post('https://r-ciclo.herokuapp.com/loginEmployee', formData)
            .then(res => {
                document.cookie = res.data.token;
                if (res.data.result) {
                    history.push('/employee')
                    window.location.reload(false)
                }
            })
            .catch((err) => { console.log(err); })
    }

    return(
        <div className="c-login">

            <div className="c-login__logo">
                 <img src={Rciclo} alt=""/>
            </div>

            <div className="c-login__container">


                <div className="c-login__top">
                    <span className="i-truck i-truck--active"/>
                    <h2 className="c-login__tittle">Bienvenido</h2>
                </div>

                <form onSubmit={handleSubmit(onSubmit)} className="c-login__form">

                    <div className="c-login__mail my-2">
                        
                        <label htmlFor="email">Usuario:</label>
                        <input className="b-input b-input--large b-input--white" type="email" id="email" name="email" placeholder="Correo electrónico" ref={register({required: true})}/>
                        {errors.mail && <span className="c-contact__span">Introduzca un email valido.</span>}

                    </div>

                    <div className="c-login__password my-2">
                    
                        <label htmlFor="password">Contraseña:</label>
                        <input className="b-input b-input--large b-input--white" type="password" id="password" name="password" placeholder="Contraseña" ref={register({required: true})}/>
                        {errors.password && <span className="c-contact__span">Contraseña incorrecta.</span>}

                    </div>

                    <div className="c-login__check">

                        {/* <label className="b-checkbox">
                
                            <input className="b-checkbox__box" type="checkbox" name="categories" ref={register()}/>
                            <span className="b-checkbox__check"/> Recordar datos.

                        </label> */}

                     </div>
                    
                     <div  className="c-login__btn" >
                        <input type="submit" className="b-btn b-btn--dark" value="Login"/>
                    </div>
                
                </form>


            </div>


        </div>
    )
}